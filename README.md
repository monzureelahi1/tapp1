# T App API Proxy

NGINX proxy app for our T app API

## Usage

### Environment Vairables

* 'LISTEN_PORT' - Port to Listen on (default: '8000')
* 'APP_HOST' - Hostname of the app to forward requests to (default: 'app')
* 'APP_PORT' - Post of the app to forward requests to (default: '9000')